class AddFieldsTimeentry < ActiveRecord::Migration[5.1]
  def change
    add_column :time_tracks, :entry_time_start, :datetime
    add_column :time_tracks, :entry_time_end, :datetime
    add_column :time_tracks, :entry_time_summary, :float
  end
end

module TimeTracksHelper
  def format_time_summary(entry_time_summary)
    hours = (entry_time_summary / 3600)
    minutes = ((entry_time_summary % 3600) / 60)
    seconds = (((entry_time_summary % 3600) % 60))

    return number_with_precision(hours,precision: 0 ).to_s.rjust(2,'0') + ":" + number_with_precision(minutes,precision: 0 ).to_s.rjust(2,'0') + ":" + number_with_precision(seconds,precision: 0 ).to_s.rjust(2,'0')
  end
end

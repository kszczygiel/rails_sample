class User < ApplicationRecord
  has_many :time_tracks
  validates :name, presence: true
  validates :email, presence: true
  validates :age, presence:true
  validates :phone, presence:true
end

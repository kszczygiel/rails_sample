json.extract! time_track, :id, :content, :user_id, :created_at, :updated_at
json.url time_track_url(time_track, format: :json)

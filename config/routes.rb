Rails.application.routes.draw do

  resources :time_tracks

  resources :users do
    member do
      get :add_entry
    end
  end

  get 'logs/login'

  root 'users#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
